function [ bit_mask ] = border_regions( mask )
%BORDER_REGIONS Compute the border regions of the given mask.
%   Works best if the mask is not along an edge of the target image.
%   The returned value bit_mask contains 0 where there was 0 in the
%   original mask, 15 in the interior of the mask, and some number between
%   1 and 14 for the border of the mask using a bitmask technique:
%
%  bit 1 (1) => north edge occupied
%  bit 2 (2) => east edge occupied
%  bit 3 (4) => south edge occupied
%  bit 4 (8) => west edge occupied
%
%   For example, the following configuration would return 12 = b1100.
%
%   1
%  0*1  => 12 (southwest corner, or north and east edges occupied)
%   0

% Initialize to the zero matrix
bit_mask = zeros(size(mask));

% North Neighbors
bit_mask = bitor(bit_mask, 1.*circshift(mask, [1, 0]));

% East Neighbors
bit_mask = bitor(bit_mask, 2.*circshift(mask, [0, -1]));

% South Neighbors
bit_mask = bitor(bit_mask, 4.*circshift(mask, [-1, 0]));

% West Neighbors
bit_mask = bitor(bit_mask, 8.*circshift(mask, [0, 1]));

% Hide cells that were not part of the original mask
bit_mask = bitand(bit_mask, 15.*mask);

end







