function [ im_poisson ] = poisson_blend( im_tgt, im_src_tgt, mask_tgt )
%POISSON_BLEND Perform Poisson blending on one channel.
%   im_src_tgt MUST be a 2-D matrix (not a 3-D matrix) for
%   this function to work.

% Map pixel coordinates to variable numbers using a lambda fn
[imh,imw] = size(im_src_tgt);
im2var = @(x,y) y + imh*(x-1);

% Start the setup process for sparse matrix A
Ax = []; Ay = []; Az = [];
[jj,ii] = find(mask_tgt);
Neq = length(ii);
eqii = (1:Neq)';
b = zeros(Neq, 1);

% Load in the variables for the ith cell
vars = im2var(ii,jj);
Ax = [Ax; eqii];
Ay = [Ay; vars];
Az = [Az; 4.*ones(size(eqii))];
b = b + 4.*im_src_tgt(vars);

% Convert the 1/0 mask to a bit-encoded border mask
% (see border_regions.m for an explanation)
bmask_tgt = border_regions(mask_tgt);
bmask_tgt_lin = bmask_tgt(bmask_tgt>0); % linear/vector form

% Northern edge equations
neighbors = (bitand(bmask_tgt_lin,1)>0);
vars = im2var(ii,jj-1);
Ax = [Ax; eqii];
Ay = [Ay; vars];
Az = [Az; -1 .* neighbors]; % calculated image pixel j
b = b + -1.*im_src_tgt(vars); % source image pixel j
b = b + 1.*(~neighbors).*im_tgt(vars); % target image pixel j

% Eastern edge equations
neighbors = (bitand(bmask_tgt_lin,2)>0);
vars = im2var(ii+1,jj);
Ax = [Ax; eqii];
Ay = [Ay; vars];
Az = [Az; -1 .* neighbors]; % calculated image pixel j
b = b + -1.*im_src_tgt(vars); % source image pixel j
b = b + 1.*(~neighbors).*im_tgt(vars); % target image pixel j

% Southern edge equations
neighbors = (bitand(bmask_tgt_lin,4)>0);
vars = im2var(ii,jj+1);
Ax = [Ax; eqii];
Ay = [Ay; vars];
Az = [Az; -1 .* neighbors]; % calculated image pixel j
b = b + -1.*im_src_tgt(vars); % source image pixel j
b = b + 1.*(~neighbors).*im_tgt(vars); % target image pixel j

% Western edge equations
neighbors = (bitand(bmask_tgt_lin,8)>0);
vars = im2var(ii-1,jj);
Ax = [Ax; eqii];
Ay = [Ay; vars];
Az = [Az; -1 .* neighbors]; % calculated image pixel j
b = b + -1.*im_src_tgt(vars); % source image pixel j
b = b + 1.*(~neighbors).*im_tgt(vars); % target image pixel j

% Construct the matrix
A = sparse(Ax, Ay, Az, Neq, imh*imw);

% Solve the matrix
v = A\b;

% Merge results into the result image
vars = im2var(ii,jj);
im_poisson = im_tgt;
im_poisson(vars) = v(vars);

% Scale between 0 and 1
im_poisson(im_poisson<0) = 0;
im_poisson(im_poisson>1) = 1;

end

