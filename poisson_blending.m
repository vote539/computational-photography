% % Poisson Blending (60 pts)
clear all;
addpath(genpath('.'));

% Function to prompt for and load an image
prompt_for_im = @(title, default) imread(uigetfile({ ...
	'*.jpg;*.tif;*.png;*.gif','All Image Files'; ...
	'*.*','All Files' }, title, ...
	sprintf('proj2_starter/samples/%s', default)));

% Load the images
im_src = im2double(prompt_for_im('Choose source image', 'penguin.jpg'));
im_tgt = im2double(prompt_for_im('Choose destination image', 'im2.JPG'));
[imh,imw,~] = size(im_tgt);

% Ask the user for the mask and alignment
mask_src = getMask(im_src);
[im_src_tgt, mask_tgt] = alignSource(im_src, mask_src, im_tgt);

% Compute Poisson blended images on each channel
im_poisson = im_tgt;
h = waitbar(0, 'Processing...');
for k=1:3
	im_poisson(:,:,k) = ...
		poisson_blend(im_tgt(:,:,k), im_src_tgt(:,:,k), mask_tgt);
	waitbar(k/3);
end
close(h);

% Show the image
imagesc(im_poisson);

% Save the image
imwrite(im_poisson, uiputfile({'*.jpg;*.tif;*.png;*.gif','All Image Files';...
	'*.*','All Files' },'Save Image',...
	'out.png'));

