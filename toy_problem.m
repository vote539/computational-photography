% Toy Problem (20 pts)
clear all;

% Load the image
im = im2double(imread('proj2_starter/samples/toy_problem.png'));
[imh,imw] = size(im);

% Map pixel coordinates to variable numbers using a lambda fn
im2var = @(x,y) y + imh*(x-1);

% Set up matrix of linear equations
Neq = imh*imw*2 - imh - imw + 1;
A = sparse(Neq, imh*imw);
b = zeros(Neq, 1);

% Populate the matrix
ei = 1;
for x=1:imw
	for y=1:imh
		% Objective 1
		if x<imw
			A(ei, im2var(x+1,y)) = 1;
			A(ei, im2var(x,y)) = -1;
			b(ei) = im(y,x+1) - im(y,x);
			ei = ei + 1;
		end
		
		% Objective 2
		if y<imh
			A(ei, im2var(x,y+1)) = 1;
			A(ei, im2var(x,y)) = -1;
			b(ei) = im(y+1,x) - im(y,x);
			ei = ei + 1;
		end
	end
end
% Objective 3
A(ei, im2var(1,1)) = 1;
b(ei) = im(1,1);

% Solve the system of equations
v = A\b;

% Display the image
colormap('gray');
imagesc(reshape(v,[imh,imw]));
















